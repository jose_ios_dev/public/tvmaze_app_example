//
//  Schedule.swift
//  TVMaze_App_Example
//
//  Created by José Caballero on 20/03/24.
//

import Foundation

struct Schedule: Codable {
    let time: String?
    let days: [String]?
}
