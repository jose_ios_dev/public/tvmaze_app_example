//
//  Country.swift
//  TVMaze_App_Example
//
//  Created by José Caballero on 20/03/24.
//

import Foundation

struct Country: Codable {
    let name: String?
    let code: String?
    let timezone: String?
}
